﻿using UnityEngine;

public class CardController : MonoBehaviour {

    public int id;
    public bool canFlip;
    private Animator anim;
    
    private void Start() {
        anim = GetComponentInParent<Animator>();
        canFlip = true;
    }

    private void OnMouseDown() {
        if (canFlip) {
            RotateCardAnim("flip", true);
        }
    }

    public void RotateCardAnim(string nameParam, bool param) {
        anim.SetBool(nameParam, param);
    }

    void FlipComplete() {
        GameManager.instance.CheckCards(this);        
    }

    public void SetCardId(int _id) {
        id = _id;
    }
}
