﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public Texture[] cardTextures;

    [SerializeField] private MsjController msjDdh;
    [SerializeField] private float showMsjTime;
    private CardController lastCard;
    private CardController cardReference;
    private GUIController hud;
    private CardSpawner cardSpawn;
    private int cardCounter;
    private int phaseCounter;
    private int score;
    private int goalScore;
    

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
        score = 0;
        cardCounter = 0;
        lastCard = GetComponent<CardController>();
        hud = GetComponent<GUIController>();
        cardSpawn = GetComponent<CardSpawner>();
        cardReference = lastCard;
        phaseCounter = 0;
        SetGoalScore(3);
        msjDdh.gameObject.SetActive(false);
        //cardSpawn.SpawnCards();
    }

    public void CheckCards(CardController card) {
        //Debug.Log("IDs *** card: " + card.id + " - lastCard: " + lastCard.id);
        cardCounter++;
        hud.UpdateTouchText("Toques: " + cardCounter);
        if (phaseCounter <= 1) {
            if (lastCard.id != card.id) {
                if (lastCard.id != 0) {
                    lastCard.RotateCardAnim("flip", false);
                    card.RotateCardAnim("flip", false);
                }
                lastCard = card;
            } else {
                score++;
                hud.UpdateScoreText("Puntaje: " + score);
                hud.UpdateDDHText(card.id - 1);
                card.canFlip = false;
                lastCard.canFlip = false;
                StartCoroutine(ShowDDHSprite(card.id-1));
                if (RoundGoal()) {
                    //Debug.Log("la ronda se termino");
                    EndRound();
                }
            }
            phaseCounter++;
            //Debug.Log("Incremento Phase Counter a " + phaseCounter);
            if (phaseCounter > 1) {
                EndPhase();
                //Debug.Log("Fin fase, reinicio Phase Counter a " + phaseCounter);                
            }
        } 
    }

    public void SetGoalScore(int _goalscore) {
        goalScore = _goalscore;
    }
        
    private void EndPhase() {
        phaseCounter = 0;
        lastCard = cardReference;        
    }

    private bool RoundGoal() {
        if (score >= goalScore) {
            return true;
        } else {
            return false;
        }
    }

    private void EndRound() {
        Debug.Log(cardCounter);
    }

    IEnumerator ShowDDHSprite(int _index) {
        msjDdh.gameObject.SetActive(true);
        msjDdh.SetSpriteMsj(_index);
        yield return new WaitForSeconds(showMsjTime);
        msjDdh.gameObject.SetActive(false);
    }
}
