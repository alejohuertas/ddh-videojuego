﻿using UnityEngine;
using UnityEngine.UI;

public class MsjController : MonoBehaviour {

    public Sprite[] msjSprite;
    private Image myImg;

    private void Awake() {
        myImg = GetComponent<Image>();
    }

    public void SetSpriteMsj(int _index) {
        myImg.sprite = msjSprite[_index];
    }
}
