﻿using UnityEngine;

public class CardSpawner : MonoBehaviour {

    [SerializeField] private GameObject card;
    [SerializeField] private Transform[] spawnPos;

    public void SpawnCards() {
        for (int i = 0; i < spawnPos.Length; i++) {
            Instantiate(card, spawnPos[i].position, Quaternion.identity);
            card.GetComponent<CardController>().SetCardId(i + 1);
        }
    }
}
