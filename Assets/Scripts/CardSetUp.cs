﻿using UnityEngine;

public class CardSetUp : MonoBehaviour {
    /*
     *  La configuracion inicial para las tarjetas respecto a temas visuales.
     */
    private CardController card;
    private Renderer rend;

	void Start () {
        card = GetComponent<CardController>();
        rend = GetComponent<Renderer>();
        SetTextureToCard();
	}

	public void SetTextureToCard() {
        rend.materials[1].SetTexture ("_MainTex" , GameManager.instance.cardTextures[card.id -1]);
    }
}
