﻿using UnityEngine;
using TMPro;

public class GUIController : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI touchText;
    [SerializeField] private TextMeshProUGUI ddhText;
    [SerializeField] private string[] ddhMsj;

    public void UpdateScoreText(string _scoreText) {
        scoreText.text = _scoreText;
    }

    public void UpdateTouchText(string _touchText) {
        touchText.text = _touchText;
    }

    public void UpdateDDHText(int _index) {
        ddhText.text = ddhMsj[_index];
    }
}
