﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject libraryPanel;
    [SerializeField] private GameObject optionsPanel;

    private void Start() {
        ShowThisPanel(mainPanel);
    }

    public void HideAllPanels() {
        mainPanel.SetActive(false);
        libraryPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }

    public void ShowThisPanel(GameObject goShow) {
        HideAllPanels();
        if (!goShow.activeInHierarchy) {
            goShow.SetActive(true);
        }
    }

    public void ChangeScene(string sc) {
        SceneManager.LoadScene(sc);
    }
}

